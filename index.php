<?php
include_once 'adminpanel/model/CommonClass.php';
$model = new CommonClass();

$about = $model->details_by_cond('text', 'category_id = 2 ORDER BY `text`.`id` DESC');

?>

<!DOCTYPE html>
<html lang="en">


<head>
	<meta charset="UTF-8">
	<title>Lgenda iT</title> 

	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">

	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">

</head>
<body>

<div class="boxed_wrapper">

<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="main-logo">
                    <a href="index.php"><img src="images/logo/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-9 menu-column">
                <nav class="menuzord" id="main_menu">
                   <ul class="menuzord-menu">
                        <li><a href="#">Home</a></li>

                        <li><a href="#">About us</a>
                        <ul class="dropdown">
                            <li><a href="about.php">About us</a></li>
                            <li><a href="our-projects.php">our projects</a></li>

                            <li><a href="team.php">Meet Our Team</a></li>
                            <li><a href="faq.php">Customers FAQ’s</a></li>
                            <li><a href="testimonials.php">Testimonials</a></li>
                         </ul>
                        </li>

                        <li><a href="#">Our services</a></li>

                      
                        <li><a href="contact.php">Contact us</a>
                       
                        </li>
                    </ul><!-- End of .menuzord-menu -->
                </nav> <!-- End of #main_menu -->
            </div>
            <div class="right-column">
                <div class="right-area">
                  
                   <div class="link_btn float_right">
                       <a href="#" class="thm-btn">Webmail</a>
                   </div>
                </div>
                    
            </div>
        </div>
                

   </div> <!-- End of .conatiner -->
</section>

<!--Start rev slider wrapper-->     
<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
            
            <li data-transition="fade">
                <img src="images/slider/1.jpg"  alt="" width="1920" height="550" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="125" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>We help businesses <br>innovate and grow.</h1>
                        <p>With over 10 years of experience helping businesses to find <br> comprehensive solutions. </p>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="364" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn yellow-bg" href="#">Meet Experts</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="188" 
                    data-y="top" data-voffset="364" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn our-solution" href="#">contact us</a>     
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="images/slider/2.jpg"  alt="" width="1920" height="580" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme"
                    data-x="right" data-hoffset="15" 
                    data-y="center" data-voffset="-4" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box">
                        <h1>Growth doesn’t <br>come without care</h1>
                        <p>With over 10 years of experience helping businesses to find <br>comprehensive solutions. </p>
                        <div class="button">
                            <a class="thm-btn yellow-bg" href="#">testimonials</a>    
                            <a class="thm-btn our-solution" href="#">contact us</a>    
                        </div>
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="images/slider/3.jpg"  alt="" width="1920" height="580" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="125" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    <div class="slide-content-box">
                        <h1>We works with focus <br>and innovative</h1>
                        <p>With over 10 years of experience helping businesses to find <br>comprehensive solutions. </p>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="364" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn yellow-bg" href="#">our company</a>     
                        </div>
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="188" 
                    data-y="top" data-voffset="364" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        <div class="button">
                            <a class="thm-btn our-solution" href="#">contact us</a>     
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<!--End rev slider wrapper--> 

<section class="about-faq sec-padd">
    <div class="container">
        <div class="section-title center">
            <h2>About us </h2>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="about-info">
                    <div class="text">
                        <?php echo $about['description']; ?>
                    </div>
                  
                </div>
            </div>

        </div>
    </div>
</section>

<section class="why-choose sec-padd">
    <div class="container">
        <div class="section-title center">
            <h2>Why Choose Our Support</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <figure class="img-box">
                        <a href="#"><img src="images/resource/1.jpg" alt=""></a>
                        <div class="overlay-box">
                            <div class="inner-box">
                                <div class="icon_box">
                                    <span class="icon-graphic"></span>
                                </div>
                                <h4>About Business</h4>
                                <div class="text">
                                    <p>Business analytics (BA) is the practice of iterative, methodical exploration of an organization's data with emphasis on statistical analysis. </p>
                                </div>
                            </div>
                        </div>
                    </figure>  
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <figure class="img-box">
                        <a href="#"><img src="images/resource/2.jpg" alt=""></a>
                        <div class="overlay-box">
                            <div class="inner-box">
                                <div class="icon_box">
                                    <span class="icon-layers"></span>
                                </div>
                                <h4>Advanced Analytics</h4>
                                <div class="text">
                                    <p>Business analytics (BA) is the practice of iterative, methodical exploration of an organization's data with emphasis on statistical analysis. </p>
                                </div>
                            </div>
                        </div>
                    </figure>  
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item">
                    <figure class="img-box">
                        <a href="#"><img src="images/resource/3.jpg" alt=""></a>
                        <div class="overlay-box">
                            <div class="inner-box">
                                <div class="icon_box">
                                    <span class="icon-computer"></span>
                                </div>
                                <h4>Customer Insights</h4>
                                <div class="text">
                                    <p>Business analytics (BA) is the practice of iterative, methodical exploration of an organizations data with emphasis on statistical analysis. </p>
                                </div>
                            </div>
                        </div>
                    </figure>  
                </div>
            </div>

        </div>
    </div>
</section>

<div class="container"><div class="border-bottom"></div></div>

<section class="testimonials-section sec-padd">
    <div class="container">
        <div class="section-title center">
            <h2>testimonials</h2>
        </div> 
        
        <!--Slider-->      
        <div class="testimonials-slider column-carousel three-column">
            
            <!--Slide-->
            <article class="slide-item">
                <div class="quote"><span class="icon-left"></span></div>
                <div class="author">
                    <div class="img-box">
                        <a href="#"><img src="images/resource/thumb1.png" alt=""></a>
                    </div>
                    <h4>Jenifer Hearly</h4>
                    <a href="#"><p>Newyork</p></a>
                </div>
                
                <div class="slide-text">
                    <p>Fortune has helped us to just have a better handle on everything in our business – to actually make decisions and move forward to grow.</p>
                </div>
            </article>
            
            <!--Slide-->
            <article class="slide-item">
                <div class="quote"><span class="icon-left"></span></div>
                <div class="author">
                    <div class="img-box">
                        <a href="#"><img src="images/resource/thumb2.png" alt=""></a>
                    </div>
                    <h4>Mitchel Harward</h4>
                    <a href="#"><p>San Fransisco</p></a>
                </div>
                
                <div class="slide-text">
                    <p>They bring a wealth of knowledge as well as a personal touch so often missing from other firms, helped us to just have better handle on everything.</p>
                </div>
            </article>
            
            <!--Slide-->
            <article class="slide-item">
                <div class="quote"><span class="icon-left"></span></div>
                <div class="author">
                    <div class="img-box">
                        <a href="#"><img src="images/resource/thumb3.png" alt=""></a>
                    </div>
                    <h4>Beally Russel</h4>
                    <a href="#"><p>Newyork</p></a>
                </div>
                
                <div class="slide-text">
                    <p>It involves an examination of operations which allows their team discuss the art of the possible. They bring a wealth of knowledge, we believe fortune.</p>
                </div>
            </article>

     
        </div>
        
    </div>    
</section>

<section class="latest-project sec-padd">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="section-title">
                    <h2>latest projects</h2>
                </div>
                <div class="text">
                    <p>We have done more than 10,000 projects in the short period time and <br>all the projects are done by our expert team with 100% satisfaction.</p>
                </div>
                <div class="link">
                    <a href="#" class="default_link">More projects <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="latest-project-carousel owl-carousel owl-theme">
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="img-box">
                                <img src="images/resource/4.jpg" alt="Awesome Image"/>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <div class="top">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                            </div><!-- /.top -->
                                            <div class="bottom">
                                                <div class="title center">
                                                    <h3>Retail Trading</h3>
                                                    <span>Finance</span>
                                                </div>
                                            </div>
                                        </div><!-- /.content -->
                                    </div><!-- /.box -->
                                </div><!-- /.overlay -->
                            </div><!-- /.img-box -->
                        </div><!-- /.single-latest-project-carousel -->
                    </div>
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="img-box">
                                <img src="images/resource/5.jpg" alt="Awesome Image"/>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <div class="top">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                            </div><!-- /.top -->
                                            <div class="bottom">
                                                <div class="title center">
                                                    <h3>Retail Trading</h3>
                                                    <span>Finance</span>
                                                </div>
                                            </div>
                                        </div><!-- /.content -->
                                    </div><!-- /.box -->
                                </div><!-- /.overlay -->
                            </div><!-- /.img-box -->
                        </div><!-- /.single-latest-project-carousel -->
                    </div>
                    <div class="item">
                        <div class="single-latest-project-carousel">
                            <div class="img-box">
                                <img src="images/resource/6.jpg" alt="Awesome Image"/>
                                <div class="overlay">
                                    <div class="box">
                                        <div class="content">
                                            <div class="top">
                                                <a href="#"><i class="fa fa-link"></i></a>
                                            </div>
                                            <div class="bottom">
                                                <div class="title center">
                                                    <h3>Retail Trading</h3>
                                                    <span>Finance</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
                
    </div>
</section>



<section class="blog-section sec-padd-top">
    <div class="container">
        <div class="section-title center">
            <h2>latest news</h2>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="img-box">
                        <a href="blog-details.php"><img src="images/blog/1.jpg" alt="News"></a>
                    </div>
                    <div class="lower-content">
                        <div class="post-meta">by fletcher  |  April 21, 2016</div>
                        <div class="text">
                            <h4><a href="blog-details.php">Retail banks wake up to digital</a></h4>
                            <p>know how to pursue pleasure rationally seds encounter consequences.</p>                            
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="img-box">
                        <a href="blog-details.php"><img src="images/blog/2.jpg" alt="News"></a>
                    </div>
                    <div class="lower-content">
                        <div class="post-meta">by fletcher  |  April 21, 2016</div>
                        <div class="text">
                            <h4><a href="blog-details.php">Create great WordPress theme</a></h4>
                            <p>Desires to obtain pain ut of itself, because it is pain because occasionally.</p>                            
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="img-box">
                        <a href="blog-details.php"><img src="images/blog/3.jpg" alt="News"></a>
                    </div>
                    <div class="lower-content">
                        <div class="post-meta">by fletcher  |  April 21, 2016</div>
                        <div class="text">
                            <h4><a href="blog-details.php">How to improve employees skills</a></h4>
                            <p>Great pleasure to take a trivial example, which of us undertakes laborious.</p>                            
                        </div>
                        
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
</section>

<div class="container"><div class="border-bottom"></div></div>

<section class="clients-section sec-padd">
    <div class="container">
        <div class="section-title">
            <h2>our partners</h2>
        </div>
        <div class="client-carousel owl-carousel owl-theme">

            <div class="item tool_tip" title="media partner">
                <img src="images/clients/1.png" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/2.png" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/3.png" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/4.png" alt="Awesome Image">
            </div>
            <div class="item tool_tip" title="media partner">
                <img src="images/clients/5.png" alt="Awesome Image">
            </div>

        </div>
    </div>  
</section>


<footer class="main-footer">
    
    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <figure class="footer-logo"><a href="index.php"><img src="images/logo/logo2.png" alt=""></a></figure>
                                
                                <div class="widget-content">
                                    <div class="text"><p>Our team offers the most up-to-date, sustainable manufacturing solutions. teachings of the great explorer of the truth We only source materials from tried and trusted suppliers.</p> </div>
                                    <div class="link">
                                        <a href="#" class="default_link">More About us <i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <div class="section-title">
                                    <h3>what we do</h3>
                                </div>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="service-single.php">Business Growth</a></li>
                                        <li><a href="service-single.php">Sustainability</a></li>
                                        <li><a href="service-single.php">Performance</a></li>
                                        <li><a href="service-single.php">Customer Insights</a></li>
                                        <li><a href="service-single.php">Organization</a></li>
                                        <li><a href="service-single.php">Advanced Analytics</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Big Column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget posts-widget">
                                <div class="section-title">
                                    <h3>latest news</h3>
                                </div>
                                <div class="widget-content">
                                    <!--Post-->
                                    <div class="post">
                                        <div class="content">
                                            <h4><a href="#">Seminar for improve your business <br>profit & loss</a></h4>
                                        </div>
                                        <div class="time">August 21, 2016</div>
                                    </div>
                                    <!--Post-->
                                    <div class="post">
                                        <div class="content">
                                            <h4><a href="#">Experts Openion for save money <br>for your future.</a></h4>
                                        </div>
                                        <div class="time">March 14, 2016</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--Footer Column-->
                        <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                            <div class="footer-widget contact-widget">
                                <div class="section-title">
                                    <h3>contact us</h3>
                                </div>
                                <div class="widget-content">
                                    <ul class="contact-info">
                                        <li><span class="icon fa fa-phone"></span>22/121 Apple Street, New York, <br>NY 10012, USA</li>
                                        <li><span class="icon fa fa-envelope"></span> Phone: +123-456-7890</li>
                                        <li><span class="icon fa fa-paper-plane"></span>Mail@Fortuneteam.com</li>
                                    </ul>
                                </div>
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                
             </div>
         </div>
     </div>
     
     <!--Footer Bottom-->
     <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
                <p>Copyrights © 2016 All Rights Reserved by <a href="#"> SteelThemes.</a></p>
                
            </div><!-- /.pull-right -->
            <div class="pull-right get-text">
                <ul>
                    <li><a href="#">Legal</a></li>
                    <li><a href="#">Sitemap</a></li>
                    <li><a href="#"> Privacy Policy</a></li>
                    <li><a href="#">Terms & Condition</a></li>
                </ul>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </section>
     
</footer>

<!-- Scroll Top Button -->
	<button class="scroll-top tran3s color2_bg">
		<span class="fa fa-angle-up"></span>
	</button>
	<!-- pre loader  -->
	<div class="preloader"></div>


	<!-- jQuery js -->
	<script src="js/jquery.js"></script>
	<!-- bootstrap js -->
	<script src="js/bootstrap.min.js"></script>
	<!-- jQuery ui js -->
	<script src="js/jquery-ui.js"></script>
	<!-- owl carousel js -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- jQuery validation -->
	<script src="js/jquery.validate.min.js"></script>
	<!-- google map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script> 
	<script src="js/gmap.js"></script>
	<!-- mixit up -->
	<script src="js/wow.js"></script>
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.fitvids.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
	<script src="js/menuzord.js"></script>

	<!-- revolution slider js -->
	<script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>
	<script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>

	<!-- fancy box -->
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.polyglot.language.switcher.js"></script>
	<script src="js/nouislider.js"></script>
	<script src="js/jquery.bootstrap-touchspin.js"></script>
	<script src="js/SmoothScroll.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/imagezoom.js"></script>	
	<script id="map-script" src="js/default-map.js"></script>
	<script src="js/custom.js"></script>

</div>
	
</body>