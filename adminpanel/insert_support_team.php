<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

include './include/header_top.php';
include './include/sideber_menu.php';


if (isset($_POST['submit'])) {
    $error = $slider->insertSupportTeam($_POST, $_FILES);
}
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="box-body">

            <div class="row">
                <div>
                    <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Insert Support Team ';?> </h2>
                </div>
                <form class="form-horizontal" method="POST"  enctype="multipart/form-data">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="form-group">
                            <label>Name : </label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Member Name.....">
                        </div>

                        <div class="form-group">
                            <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                        </div>
                        <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                    </div>

                </form>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->
    <script>
        $("#file-4").fileinput({
            allowedFileExtensions: ['jpg','JPEG', 'png', 'gif'],
            showUpload: false,
            /*initialPreview: [
             "http://lorempixel.com/1920/1080/transport/1",
             ],*/
            initialPreviewConfig: [
                {caption: "transport-1.jpg", size: 329892, width: "220px", url: "{$url}", key: 1},
            ]


        });
    </script>
<?php
include './include/footer.php';
?>