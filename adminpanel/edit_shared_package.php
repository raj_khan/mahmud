<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include './include/header_top.php';
include './include/sideber_menu.php';

include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

if (isset($_GET['sharePackage_id'])) {
    $id = $_GET['sharePackage_id'];
}


$viewAllSlider = $model->details_by_cond('package', 'id = ' . $id . '');



if (isset($_POST['submit'])) {
    $error = $slider->updateSharedPackage($_POST, $_FILES);

}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center"
                    style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Update Shared Package Info'; ?></h2>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="col-md-8 col-md-offset-2">


                    <div class="form-group">
                        <label> Package Name : </label>
                        <input type="text" name="packageName" class="form-control" value="<?php echo $viewAllSlider['name']; ?>">
                    </div>


                    <div class="form-group">
                        <label> Bandwith : </label>
                        <input type="text" name="bandwith" class="form-control" value="<?php echo $viewAllSlider['bandwith']; ?>">
                    </div>



                    <div class="form-group">
                        <label> Support : </label>
                        <input type="text" name="suport" class="form-control" value="<?php echo $viewAllSlider['suport']; ?>">
                    </div>



                    <div class="form-group">
                        <label> Youtube Bandwith : </label>
                        <input type="text" name="youtubeBandwith" class="form-control" value="<?php echo $viewAllSlider['youtube_bandwith']; ?>">
                    </div>


                    <div class="form-group">
                        <label> FTP Bandwith : </label>
                        <input type="text" name="ftpBandwith" class="form-control" value="<?php echo $viewAllSlider['ftp_bandwith']; ?>">
                    </div>


                    <input type="hidden" name="id" value="<?php echo $viewAllSlider['id'] ?>">
                    <input class="btn btn-success" type="submit" name="submit" value="Submit" style="float: right">
                </div>
            </form>

        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->
<script>
    $("#file-4").fileinput({
        allowedFileExtensions: ['jpg', 'JPEG', 'png', 'gif'],
        showUpload: false,
        /*initialPreview: [
         "http://lorempixel.com/1920/1080/transport/1",
         ],*/
        initialPreviewConfig: [
            {caption: "transport-1.jpg", size: 329892, width: "220px", url: "{$url}", key: 1},
        ]


    });
</script>
<?php
include './include/footer.php';
?>

