<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include './include/header_top.php';
include './include/sideber_menu.php';

include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

if (isset($_GET['social_id'])) {
    $id = $_GET['social_id'];
}


$viewAllSlider = $model->details_by_cond('social_links', 'id = ' . $id . '');


if (isset($_POST['submit'])) {
    $error = $slider->updateSocialLinks($_POST);

}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center"
                    style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Update Social Link'; ?></h2>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="col-md-8 col-md-offset-2">

                    <!--Description-->
                    <div class="form-group">
                        <label> Link : </label>
                        <input type="text" name="link" class="form-control" value="<?php echo $viewAllSlider['link']; ?>">
                    </div>


                    <input type="hidden" name="id" value="<?php echo $viewAllSlider['id'] ?>">
                    <input class="btn btn-success" type="submit" name="submit" value="Submit" style="float: right">
                </div>
            </form>

        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->
<script>
    $("#file-4").fileinput({
        allowedFileExtensions: ['jpg', 'JPEG', 'png', 'gif'],
        showUpload: false,
        /*initialPreview: [
         "http://lorempixel.com/1920/1080/transport/1",
         ],*/
        initialPreviewConfig: [
            {caption: "transport-1.jpg", size: 329892, width: "220px", url: "{$url}", key: 1},
        ]


    });
</script>
<?php
include './include/footer.php';
?>

