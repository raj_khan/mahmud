<?php
//session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}

include_once 'model/CommonClass.php';
$common = new CommonClass();


// Slider Delete
if(isset($_GET["slider_id"]) && !empty($_GET["slider_id"])){
    
    $id = $_GET["slider_id"];
    $del_success = $common ->deleteData('image',"id = $id");
    if($del_success){
        header("location:view_slider.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_slider.php?status=Notok");
        echo 'Fail';
    }
}




// services_id
if(isset($_GET["services_id"]) && !empty($_GET["services_id"])){

    $id = $_GET["services_id"];
    $del_success = $common ->deleteData('image',"id = $id");
    if($del_success){
        header("location:view_services.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_services.php?status=Notok");
        echo 'Fail';
    }
}



// popular_services_id
if(isset($_GET["popular_services_id"]) && !empty($_GET["popular_services_id"])){

    $id = $_GET["popular_services_id"];
    $del_success = $common ->deleteData('image',"id = $id");
    if($del_success){
        header("location:view_popular_services.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_popular_services.php?status=Notok");
        echo 'Fail';
    }
}




// support_id
if(isset($_GET["support_id"]) && !empty($_GET["support_id"])){

    $id = $_GET["support_id"];
    $del_success = $common ->deleteData('image',"id = $id");
    if($del_success){
        header("location:view_support_team.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_support_team.php?status=Notok");
        echo 'Fail';
    }
}




// social_id
if(isset($_GET["social_id"]) && !empty($_GET["social_id"])){

    $id = $_GET["social_id"];
    $del_success = $common ->deleteData('social_links',"id = $id");
    if($del_success){
        header("location:view_social_links.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_social_links.php?status=Notok");
        echo 'Fail';
    }
}




// product_id
if(isset($_GET["product_id"]) && !empty($_GET["product_id"])){

    $id = $_GET["product_id"];
    $del_success = $common ->deleteData('products',"id = $id");
    if($del_success){
        header("location:view_shared_package.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_shared_package.php?status=Notok");
        echo 'Fail';
    }
}





// about_id
if(isset($_GET["about_id"]) && !empty($_GET["about_id"])){

    $id = $_GET["about_id"];
    $del_success = $common ->deleteData('text',"id = $id");
    if($del_success){
        header("location:view_about.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_about.php?status=Notok");
        echo 'Fail';
    }
}




// currnet_Notice_id
if(isset($_GET["currnet_Notice_id"]) && !empty($_GET["currnet_Notice_id"])){

    $id = $_GET["currnet_Notice_id"];
    $del_success = $common ->deleteData('text',"id = $id");
    if($del_success){
        header("location:view_current_notice.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_current_notice.php?status=Notok");
        echo 'Fail';
    }
}




// corporate_id
if(isset($_GET["corporate_id"]) && !empty($_GET["corporate_id"])){

    $id = $_GET["corporate_id"];
    $del_success = $common ->deleteData('package',"id = $id");
    if($del_success){
        header("location:view_corporate_package.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_corporate_package .php?status=Notok");
        echo 'Fail';
    }
}



// features_id
if(isset($_GET["features_id"]) && !empty($_GET["features_id"])){

    $id = $_GET["features_id"];
    $del_success = $common ->deleteData('package',"id = $id");
    if($del_success){
        header("location:view_features_package.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_features_package .php?status=Notok");
        echo 'Fail';
    }
}



// shared_id
if(isset($_GET["shared_id"]) && !empty($_GET["shared_id"])){

    $id = $_GET["shared_id"];
    $del_success = $common ->deleteData('package',"id = $id");
    if($del_success){
        header("location:view_shared_package.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_shared_package .php?status=Notok");
        echo 'Fail';
    }
}



// features_id
if(isset($_GET["gaming_id"]) && !empty($_GET["gaming_id"])){

    $id = $_GET["gaming_id"];
    $del_success = $common ->deleteData('package',"id = $id");
    if($del_success){
        header("location:view_gaming_package.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_gaming_package .php?status=Notok");
        echo 'Fail';
    }
}





// gallery_id
if(isset($_GET["gallery_id"]) && !empty($_GET["gallery_id"])){

    $id = $_GET["gallery_id"];
    $del_success = $common ->deleteData('image',"id = $id");
    if($del_success){
        header("location:view_current_notice.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_current_notice.php?status=Notok");
        echo 'Fail';
    }
}



// welcome_id
if(isset($_GET["welcome_id"]) && !empty($_GET["welcome_id"])){

    $id = $_GET["welcome_id"];
    $del_success = $common ->deleteData('text',"id = $id");
    if($del_success){
        header("location:view_welcome.php?status=ok");
        echo 'Success';
    } else {
        header("location:view_welcome.php?status=Notok");
        echo 'Fail';
    }
}


