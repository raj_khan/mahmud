<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

include './include/header_top.php';
include './include/sideber_menu.php';


if (isset($_POST['submit'])) {
    $error = $slider->insertPackages($_POST);
}
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="box-body">

            <div class="row">
                <div>
                    <h2 class="bg-success text-primary text-center"
                        style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Insert Package'; ?> </h2>
                </div>
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="col-md-8 col-md-offset-2">


                        <!--packageName-->
                        <div class="form-group">
                            <label>Package Name : </label>
                            <input type="text" name="packageName" class="form-control" placeholder="packageName">
                        </div>


                        <!--bandwith-->
                        <div class="form-group">
                            <label>Bandwith : </label>
                            <input type="text" name="bandwith" class="form-control" placeholder="bandwith">
                        </div>


                        <!--suport-->
                        <div class="form-group">
                            <label>Support : </label>
                            <input type="text" name="suport" class="form-control" placeholder="suport">
                        </div>


                        <!--youtube_bandwith-->
                        <div class="form-group">
                            <label>Youtube Bandwith : </label>
                            <input type="text" name="youtubeBandwith" class="form-control" placeholder="Youtube Bandwith">
                        </div>

                        <!--ftp_bandwith-->
                        <div class="form-group">
                            <label>FTP BANDWITH : </label>
                            <input type="text" name="ftpBandwith" class="form-control" placeholder="FTP Bandwith">
                        </div>


                        <input type="checkbox" name="featuresPackage" value="1">Post as Features Package<br>
                        <input type="checkbox" name="sharedPackage" value="1"> Post as Shared Package <br>
                        <input type="checkbox" name="gamingPackage" value="1"> Post as Gaming Package<br>
                        <input type="checkbox" name="corporatePackage" value="1">  Post as Corporate Package<br>

                        <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                    </div>

                </form>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->

<?php
include './include/footer.php';
?>