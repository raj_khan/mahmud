<?php

include_once("Database.php");
include_once("CommonClass.php");
$model = new CommonClass();

class Slider extends Database
{

    public $con;

    public function __construct()
    {

        $this->con = parent::connect();
    }

    public function insertSlider($postData, $fileData)
    {


        $prod_title = $postData['slider_title'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $slider_description = $postData['textarea'];
        $slider_description = mysqli_real_escape_string($this->con, $slider_description);


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/images/slider_image/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);


        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `description`, `image`) VALUES (NULL, '1', '$prod_title', '$slider_description', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }




    public function insertServices($postData, $fileData)
    {


        $servicesTitle = $postData['servicesTitle'];
        $servicesTitle = mysqli_real_escape_string($this->con, $servicesTitle);


        $servicesDescription = $postData['servicesDescription'];
        $servicesDescription = mysqli_real_escape_string($this->con, $servicesDescription);

        $popularServices = $postData['popularServices'];


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/images/services_image/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);


        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `description`, `image`, `popuar_services`) VALUES (NULL, '4', '$servicesTitle', '$servicesDescription', '$file_name', '$popularServices')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }



    public function insertSupportTeam($postData, $fileData)
    {


        $name = $postData['name'];
        $name = mysqli_real_escape_string($this->con, $name);



        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/images/support_team/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);


        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `image`) VALUES (NULL, '6', '$name', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }


    public function showSliderImages()
    {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `image` WHERE `category_id` = 1  ORDER BY `image`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 4) {
                break;
            }
            $returnData[] = $row;
            $i++;
        }
        return $returnData;
    }

    public function showForEdit()
    {

        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 1  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }

    public function showSliderCategory()
    {

        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `slider_table`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row_for_category = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $row_for_category['category'] . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
            $showCategoryName = mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            $returnData = $showCategoryName;
        }
        return $returnData;
    }

    public function showCategory()
    {
        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `category_id_name`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }
        return $returnData;
    }

    public function showSingleSlider($id)
    {

        $queryForSingleSlider = "SELECT * FROM `slider_table` WHERE `id` = $id";
        $SingleSlider = mysqli_query($this->con, $queryForSingleSlider);
        $returnData = mysqli_fetch_array($SingleSlider, MYSQLI_ASSOC);

        return $returnData;
    }

    public function updateSlider($postData, $fileData)
    {


        $prod_catagory = $postData['prod_catogory'];
        $prod_title = $postData['add_title'];
        $id = $postData['id'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $singleSlider = $this->showSingleSlider($id);

        if ($prod_title == "") {
            $error = "Please input Image Title";
        }
        if ($prod_catagory == "") {
            $error = "Please input Product Category";
        } else if ($fileData['file']['size'] > 250000) {
            $error = "Please insert image not more than 250 KB";
        }

        if (isset($fileData['file']['name']) && !empty($fileData['file']['name'])) {
            $file_name = uniqid() . $fileData['file']['name'];
            $destination = "../img/all-images/" . $file_name;
            $upload = move_uploaded_file($fileData['file']['tmp_name'], $destination);
        } else {
            $file_name = $singleSlider['img_link'];
        }

        $product_update_query = "UPDATE `slider_table` SET `category` = '$prod_catagory', `title` = '$prod_title', `img_link` = '$file_name' WHERE `slider_table`.`id` = $id";

        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());
        if ($update_query) {
            ?>
            <script>
                window.location = "viewSlider.php";
            </script>
            <?php
        } else {
            $error = "Error! Could not Upload your Information";

        }
        return $update_query;
    }


    //insert insertProducts

    public function insertPackages($postData)
    {


        $packageName = $postData['packageName'];

        $bandwith = $postData['bandwith'];
        $suport = $postData['suport'];
        $suport = mysqli_real_escape_string($this->con, $suport);
        $bandwith = mysqli_real_escape_string($this->con, $bandwith);

        $youtubeBandwith = $postData['youtubeBandwith'];
        $youtubeBandwith = mysqli_real_escape_string($this->con, $youtubeBandwith);


        $ftpBandwith = $postData['ftpBandwith'];
        $ftpBandwith = mysqli_real_escape_string($this->con, $ftpBandwith);

        $featuresPackage = $postData['featuresPackage'];

        $sharedPackage = $postData['sharedPackage'];

        $gamingPackage = $postData['gamingPackage'];

        $corporatePackage = $postData['corporatePackage'];


        $product_insert_query = "INSERT INTO `package` (`id`, `name`, `bandwith`, `suport`, `youtube_bandwith`, `ftp_bandwith`, `features_package`, `shared_package`, `gaming_package`, `corporate_package`) VALUES (NULL, '$packageName', '$bandwith', '$suport', '$youtubeBandwith', '$ftpBandwith', '$featuresPackage', '$sharedPackage', '$gamingPackage', '$corporatePackage')";


        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }



    //insert insertProducts

    public function insertSocialLinks($postData)
    {



        $link = $postData['link'];
        $link = mysqli_real_escape_string($this->con, $link);

        $facebook = $postData['facebook'];
        $youtube = $postData['youtube'];
        $linkedin = $postData['linkedin'];
        $googleplus = $postData['googleplus'];


        $product_insert_query = "INSERT INTO `social_links` (`id`, `link`, `facebook`, `youtube`, `linkedin`, `google_plus`) VALUES (NULL, '$link', '$facebook', '$youtube', '$linkedin', '$googleplus')";


        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }




    //insertWelcome
    public function insertWelcome($postData)
    {

        $welcomeMessage = $postData['message'];
        $welcomeMessage = mysqli_real_escape_string($this->con, $welcomeMessage);




        $product_insert_query = "INSERT INTO `text` (`id`, `category_id`, `description`) VALUES (NULL, '2', '$welcomeMessage')";


        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your Welcome Message ";
        } else {
            return "Error! Could not Upload your  Welcome Message";
        }
    }






    // insert  About
    public function insertAbout($postData)
    {

        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);



        $product_insert_query = "INSERT INTO `text` (`id`, `category_id`, `description`) VALUES (NULL, '2', '$description')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your About information ";
        } else {
            return "Error! Could not Upload your About Information";
        }
    }



    // insert  About
    public function insertCurrentNotice($postData)
    {

        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);



        $product_insert_query = "INSERT INTO `text` (`id`, `category_id`, `description`) VALUES (NULL, '3', '$description')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your About information ";
        } else {
            return "Error! Could not Upload your About Information";
        }
    }






    //insertCategory
    public function insertCategory($postData)
    {

        $name = $postData['name'];
        $name = mysqli_real_escape_string($this->con, $name);


        $product_insert_query = "INSERT INTO `category` (`id`, `category_name`) VALUES (NULL, '$name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! ADD your New Category ";
        } else {
            return "Error! Could not Add your New Category";
        }
    }





    // insert gallery
    public function insertGallery( $fileData)
    {


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../img/gallery_img/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `img`) VALUES (NULL, '4', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your Gallery  Image ";
        } else {
            return "Error! Could not Upload your Gallery  Image ";
        }
    }



//============================================INSERT END==================================//




//===================================Update Start================================//

//Update About
    public function updateAbout($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];


        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '2',
            'description' => $description
        );



        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_about.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//updateCurrentNotice
    public function updateCurrentNotice($postData)
    {
        global $model;
        $id = $postData['id'];


        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '3',
            'description' => $description
        );



        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_current_notice.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//updateCurrentNotice
    public function updateSocialLinks($postData)
    {
        global $model;
        $id = $postData['id'];

        $link = $postData['link'];
        $link = mysqli_real_escape_string($this->con, $link);

        $formData = array(
            'link' => $link
        );


        $update_query = $model->Update_data('social_links', $formData, "`social_links`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_social_links.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//Update Welcome
    public function updateWelcome($postData)
    {
        global $model;
        $id = $postData['id'];

        $description = $postData['description'];
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => 2,
            'description' => $description
        );



        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_welcome.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//updateSharedPackage
    public function updateSharedPackage($postData)
    {
        global $model;
        $id = $postData['id'];

        $packageName = $postData['packageName'];

        $bandwith = $postData['bandwith'];
        $suport = $postData['suport'];
        $suport = mysqli_real_escape_string($this->con, $suport);
        $bandwith = mysqli_real_escape_string($this->con, $bandwith);

        $youtubeBandwith = $postData['youtubeBandwith'];
        $youtubeBandwith = mysqli_real_escape_string($this->con, $youtubeBandwith);


        $ftpBandwith = $postData['ftpBandwith'];
        $ftpBandwith = mysqli_real_escape_string($this->con, $ftpBandwith);
        $formData = array(
            'name' => $packageName,
            'bandwith' => $bandwith,
            'suport' => $suport,
            'youtube_bandwith' => $youtubeBandwith,
            'ftp_bandwith' => $ftpBandwith
        );



        $update_query = $model->Update_data('package', $formData, "`package`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_shared_package.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }




//updateSharedPackage
    public function updateGamingPackage($postData)
    {
        global $model;
        $id = $postData['id'];

        $packageName = $postData['packageName'];

        $bandwith = $postData['bandwith'];
        $suport = $postData['suport'];
        $suport = mysqli_real_escape_string($this->con, $suport);
        $bandwith = mysqli_real_escape_string($this->con, $bandwith);

        $youtubeBandwith = $postData['youtubeBandwith'];
        $youtubeBandwith = mysqli_real_escape_string($this->con, $youtubeBandwith);


        $ftpBandwith = $postData['ftpBandwith'];
        $ftpBandwith = mysqli_real_escape_string($this->con, $ftpBandwith);
        $formData = array(
            'name' => $packageName,
            'bandwith' => $bandwith,
            'suport' => $suport,
            'youtube_bandwith' => $youtubeBandwith,
            'ftp_bandwith' => $ftpBandwith
        );



        $update_query = $model->Update_data('package', $formData, "`package`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_gaming_package.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }



//updateSharedPackage
    public function updateCorporatePackage($postData)
    {
        global $model;
        $id = $postData['id'];

        $packageName = $postData['packageName'];

        $bandwith = $postData['bandwith'];
        $suport = $postData['suport'];
        $suport = mysqli_real_escape_string($this->con, $suport);
        $bandwith = mysqli_real_escape_string($this->con, $bandwith);

        $youtubeBandwith = $postData['youtubeBandwith'];
        $youtubeBandwith = mysqli_real_escape_string($this->con, $youtubeBandwith);


        $ftpBandwith = $postData['ftpBandwith'];
        $ftpBandwith = mysqli_real_escape_string($this->con, $ftpBandwith);
        $formData = array(
            'name' => $packageName,
            'bandwith' => $bandwith,
            'suport' => $suport,
            'youtube_bandwith' => $youtubeBandwith,
            'ftp_bandwith' => $ftpBandwith
        );



        $update_query = $model->Update_data('package', $formData, "`package`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_corporate_package.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }






//updateSharedPackage
    public function updateFeaturesPackage($postData)
    {
        global $model;
        $id = $postData['id'];

        $packageName = $postData['packageName'];

        $bandwith = $postData['bandwith'];
        $suport = $postData['suport'];
        $suport = mysqli_real_escape_string($this->con, $suport);
        $bandwith = mysqli_real_escape_string($this->con, $bandwith);

        $youtubeBandwith = $postData['youtubeBandwith'];
        $youtubeBandwith = mysqli_real_escape_string($this->con, $youtubeBandwith);


        $ftpBandwith = $postData['ftpBandwith'];
        $ftpBandwith = mysqli_real_escape_string($this->con, $ftpBandwith);
        $formData = array(
            'name' => $packageName,
            'bandwith' => $bandwith,
            'suport' => $suport,
            'youtube_bandwith' => $youtubeBandwith,
            'ftp_bandwith' => $ftpBandwith
        );



        $update_query = $model->Update_data('package', $formData, "`package`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_features_package.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }





//updateClientInfo
    public function updateClientInfo($postData)
    {
        global $model;
        $id = $postData['id'];

        $clientName = $postData['clientName'];
        $clientName = mysqli_real_escape_string($this->con, $clientName);

        $projectName = $postData['projectName'];
        $projectName = mysqli_real_escape_string($this->con, $projectName);

        $remarks = $postData['remarks'];
        $remarks = mysqli_real_escape_string($this->con, $remarks);

        $formData = array(
            'client_name' => $clientName,
            'project_name' => $projectName,
            'remarks' => $remarks
        );


        $update_query = $model->Update_data('client', $formData, "`client`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_clientInformation.php";
            </script>
            <?php

            return "Success! Client Information Updated succesfully ";
        } else {
            return "Error! Could not Update your Client Information";
        }
    }



//updateCareers
    public function updateCareers($postData)
    {
        global $model;
        $id = $postData['id'];

        $shortDescription = $postData['shortDescription'];
        $shortDescription = mysqli_real_escape_string($this->con, $shortDescription);

        $importantFacts = $postData['importantFacts'];
        $importantFacts = mysqli_real_escape_string($this->con, $importantFacts);


        $job_title = $postData['job_title'];
        $job_title = mysqli_real_escape_string($this->con, $job_title);


        $responsibility = $postData['responsibility'];
        $responsibility = mysqli_real_escape_string($this->con, $responsibility);


        $job_type = $postData['job_type'];
        $job_type = mysqli_real_escape_string($this->con, $job_type);


        $salary_range = $postData['salary_range'];
        $salary_range = mysqli_real_escape_string($this->con, $salary_range);


        $Qualification = $postData['Qualification'];
        $Qualification = mysqli_real_escape_string($this->con, $Qualification);


        $location = $postData['location'];
        $location = mysqli_real_escape_string($this->con, $location);

        $formData = array(
            'short_description' => $shortDescription,
            'important_facts' => $importantFacts,
            'job_title' => $job_title,
            'responsibility' => $responsibility,
            'job_type' => $job_type,
            'salary_range' => $salary_range,
            'qualification' => $Qualification,
            'location' => $location
        );


        $update_query = $model->Update_data('carees', $formData, "`carees`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_careers.php";
            </script>
            <?php

            return "Success! Career Information Updated succesfully ";
        } else {
            return "Error! Could not Update your Career Information";
        }
    }



//    Slider Update
    public function updateSliderInfo($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/images/slider_image/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '1',
            'title' => $services_title,
            'description' => $description,
            'image' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['image']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_slider.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateServices
    public function updateServices($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/images/services_image/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '4',
            'title' => $services_title,
            'description' => $description,
            'image' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['image']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_services.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updatePopularServices
    public function updatePopularServices($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/images/services_image/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '4',
            'title' => $services_title,
            'description' => $description,
            'image' => $file_name,
            'popuar_services' => '1'

        );

        if ($fileExist == FALSE) {
            unset($formData['image']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_services.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }



//    updateSupportTeam
    public function updateSupportTeam($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/images/support_team/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '6',
            'title' => $services_title,
            'description' => $description,
            'image' => $file_name

        );

        if ($fileExist == FALSE) {
            unset($formData['image']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_support_team.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateOurServices
    public function updateOurServices($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/services_img/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '2',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_services.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateSocialResponsibility
    public function updateSocialResponsibility($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/socialResponsibility/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '3',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_responsibility.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateTestimonial
    public function updateTestimonial($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/testimonial/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '4',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_testimonial.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateNews
    public function updateNews($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/news/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '5',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_news.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }


//    updateWhyChooseUs
    public function updateWhyChooseUs($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '6',
            'title' => $services_title,
            'description' => $description
        );


        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_whyChooseUs.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }


//    updateSpecification
    public function updateSpecification($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '7',
            'title' => $services_title,
            'description' => $description
        );


        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_specification.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }



//Update Gallery
    public function updateGallery($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/gallery_img/');
        } else {
            $file_name = '';
        }


        $formData = array(
            'category_id' => '4',
            'img' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_gallery.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }



//    Update Partner
    public function updatePartner($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/partner/');
        } else {
            $file_name = '';
        }

        $gallery_title = $postData['title'];

        $gallery_title = mysqli_real_escape_string($this->con, $gallery_title);

        $formData = array(
            'category_id' => '9',
            'title' => $gallery_title,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_partner.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }

}
