<?php

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}

include './include/header_top.php';
include './include/sideber_menu.php';
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section id="counter" class="counter">
        <div class="main_counter_area">
            <div class="overlay p-y-3">
                <div class="container ">

                    <h1 class="text-primary text-center">Welcome To LgendaIT Admin Panel</h1>

                </div>
            </div>
        </div>
    </section><!-- End of counter Section -->




</div>
<!-- /.content-wrapper -->
<?php
include './include/footer.php';
?>