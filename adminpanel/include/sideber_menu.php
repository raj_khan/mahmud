<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header text-center" style="color: #9c6411;">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Insert</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
<!--                    <li><a href="insert_category.php"><i class="fa fa-circle-o"></i>Add Category</a></li>-->
<!--                    <li><a href="insert_package.php"><i class="fa fa-circle-o"></i>ADD PACKAGE</a></li>-->
<!--                    <li><a href="insert_slider.php"><i class="fa fa-circle-o"></i>ADD SLIDER</a></li>-->
<!---->
<!--                    <li><a href="insert_current_notice.php"><i class="fa fa-circle-o"></i> ADD CURRENT NOTICE</a></li>-->
<!--                    <li><a href="insert_welcome.php"><i class="fa fa-circle-o"></i> ADD WELCOME MESSAGE</a></li>-->
<!--                    <li><a href="insert_social_links.php"><i class="fa fa-circle-o"></i> ADD SOCIAL LINKS</a></li>-->
<!--                    <li><a href="insert_support_team.php"><i class="fa fa-circle-o"></i> ADD SUPPORT TEAM</a></li>-->
<!--                    <li><a href="insert_services.php"><i class="fa fa-circle-o"></i> ADD SERVICES </a></li>-->
                    <li><a href="insert_about.php"><i class="fa fa-circle-o"></i> ADD ABOUT</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>View</span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
            <ul class="treeview-menu">
                <li><a href="view_about.php"><i class="fa fa-circle-o"></i> View About</a></li>
<!--                <li><a href="view_shared_package.php"><i class="fa fa-circle-o"></i>View Shared Package </a></li>-->
<!--                    <li><a href="view_features_package.php"><i class="fa fa-circle-o"></i>View Features Package </a></li>-->
<!--                    <li><a href="view_gaming_package.php"><i class="fa fa-circle-o"></i>View Gaming Package </a></li>-->
<!--                    <li><a href="view_corporate_package.php"><i class="fa fa-circle-o"></i>View Corporate Package </a></li>-->
<!---->
<!--                    <li><a href="view_slider.php"><i class="fa fa-circle-o"></i>View Slider </a></li>-->
<!---->
<!--                    <li><a href="view_current_notice.php"><i class="fa fa-circle-o"></i> View Current Notice</a></li>-->
<!--                    <li><a href="view_welcome.php"><i class="fa fa-circle-o"></i> View Welcome Message</a></li>-->
<!--                    <li><a href="view_services.php"><i class="fa fa-circle-o"></i> View Services</a></li>-->
<!--                    <li><a href="view_popular_services.php"><i class="fa fa-circle-o"></i> View Popular Services</a></li>-->
<!---->
<!--                    <li><a href="view_support_team.php"><i class="fa fa-circle-o"></i> View Support Team</a></li>-->
<!--                    <li><a href="view_social_links.php"><i class="fa fa-circle-o"></i> View Social Links</a></li>-->
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>