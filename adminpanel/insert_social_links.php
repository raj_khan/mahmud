<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

include './include/header_top.php';
include './include/sideber_menu.php';


if (isset($_POST['submit'])) {
    $error = $slider->insertSocialLinks($_POST);
}
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="box-body">

            <div class="row">
                <div>
                    <h2 class="bg-success text-primary text-center"
                        style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'INSERT SOCIAL LINKS'; ?> </h2>
                </div>
                <form class="form-horizontal" method="POST">
                    <div class="col-md-8 col-md-offset-2">


                        <!--Name-->
                        <div class="form-group">
                            <label>Link : </label>
                            <input type="text" name="link" class="form-control" placeholder="Add Social Link">
                        </div>



                        <input type="checkbox" name="facebook" value="1">Link For Facebook<br>
                        <input type="checkbox" name="youtube" value="1"> Link For Youtube <br>
                        <input type="checkbox" name="linkedin" value="1"> Link For Linkedin<br>
                        <input type="checkbox" name="googleplus" value="1"> Link For Google Plus<br>



                        <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                    </div>

                </form>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->

<?php
include './include/footer.php';
?>