<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include './include/header_top.php';
include './include/sideber_menu.php';

include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

if (isset($_GET['welcome_id'])) {
    $id = $_GET['welcome_id'];
}


$viewAllSlider = $model->details_by_cond('text', 'id = ' . $id . '');




if (isset($_POST['submit'])) {
    $error = $slider->updateWelcome($_POST);

}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center"
                    style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Update Welcome Message'; ?></h2>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="col-md-8 col-md-offset-2">

                    <!--Description-->
                    <label>Description: </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="description" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $viewAllSlider['description']; ?></textarea>

                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $viewAllSlider['id'] ?>">
                    <input class="btn btn-success" type="submit" name="submit" value="Submit" style="float: right">
                </div>
            </form>

        </div>
        <!-- /.row -->


    </div>

</div>

<?php
include './include/footer.php';
?>

