<?php
include './include/header_top.php';
include './include/sideber_menu.php';
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

$allSliderImage = $model->view_all('social_links');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;">All
                    Social Links View</h2>
            </div>

            <!---=======================Data Table=====================------>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($allSliderImage as $image) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>

                                    <td>
                                        <?php echo $image['link']; ?>
                                    </td>
                                    <td>
                                        <a href="edit_social_links.php?social_id=<?php echo $image['id']; ?>">
                                            <button type="button" class="btn btn-success">Edit</button>
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to delete?')" href="deleteProduct.php?social_id=<?php echo $image['id']; ?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php
include './include/footer.php';
?>
